# frozen_string_literal: true

# SPDX-License-Identifier: AGPL-3.0-or-later
# SPDX-FileCopyrightText: 2016-2019 Harald Sitter <sitter@kde.org>

require_relative 'lib/differential'
require_relative 'lib/maniphest'
require_relative 'lib/project'

# Phabricator Plugin
class PhabricatorPlugin < Plugin
  Config.register Config::StringValue.new('phabricator.api_token',
                                          :requires_rescan => true,
                                          :default => '',
                                          :desc => 'Phabricator API token to use for conduit.')

  Config.register Config::ArrayValue.new('phabricator.blacklist',
                                          :default => %w[#kde-bugs-activity],
                                          :desc => 'Disables all message handling in the listed channels.')

  Config.register Config::ArrayValue.new('phabricator.url_blacklist',
                                          :default => [],
                                          :desc => 'Disables message handling on URLs only in the listed channels. Trigger words still get handled.')

  def initialize
    super
    Conduit::Connection.api_token = bot.config['phabricator.api_token']
  end

  def unreplied(m, _ = {})
    return if skip?(m)
    maybe_task(m) unless m.replied?
    maybe_diff(m) unless m.replied?
  end

  def maybe_task(m)
    return unless (match = m.message.scan(/\bT(\d+)\b+/))
    match.flatten.each do |number|
      task(m, number: number)
    end
  end

  def maybe_diff(m)
    return unless (match = m.message.scan(/\bD(\d+)\b/))
    match.flatten.each do |number|
      diff(m, number: number)
    end
  end

  def task(m, number:)
    task = Conduit::Maniphest.get(number)
    projects = Conduit::Project.find_by_phids(task.projectPHIDs)
    m.reply "Task #{task.id} \"#{task.title}\" [#{task.statusName},#{task.priority}] {#{projects.collect(&:name).join(',')}} #{task.uri}"
  rescue => e
    m.notify "Task not found ¯\\_(ツ)_/¯ #{e}"
  end

  def diff(m, number:)
    diff = Conduit::Differential.get(number)
    m.reply "Diff #{diff.id} \"#{diff.title}\" [#{diff.statusName}] #{diff.uri}"
  rescue => e
    m.notify "Diff not found ¯\\_(ツ)_/¯ #{e}"
  end

  private

  def skip?(m)
    skip_handling?(m) || skip_url_handling?(m)
  end

  def skip_handling?(m)
    bot.config['phabricator.blacklist'].any? do |exclude|
      m.channel && m.channel.name == exclude
    end
  end

  def skip_url_handling?(m)
    # This isn't technically the most reliable check. We'd have to find
    # matches, determine their urlyness and then skip the urly ones while
    # processing the nonurly ones, I really can't be bothered with that.
    is_url = m.message.include?('https://phabricator.kde.org/')
    return false unless is_url
    bot.config['phabricator.url_blacklist'].any? do |exclude|
      m.channel && m.channel.name == exclude
    end
  end
end

PhabricatorPlugin.new unless ENV['DONT_TEST_INIT']
