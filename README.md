<!--
SPDX-License-Identifier: CC0-1.0
SPDX-FileCopyrightText: 2016-2019 Harald Sitter <sitter@kde.org>
-->

# Installation

- Clone anywhere
- Install dependencies
  - When using Bundler: `bundle install --without=development`
  - Manually: `gem install finer_struct && gem install faraday`
- Add clone path to `~/.rbot/conf.yaml`
```
plugins.path:
  - /home/me/src/git/rbot-phabricator
```
- Restart bot
- Generate an API token on Phabricator https://phabricator.kde.org/settings/panel/apitokens/
- Set API token in bot config via IRC query
```
[13:03] <sitter> config set phabricator.api_token api-asdfasdfafasdfsdafs
[13:03] <sittingbot> this config change will take effect on the next rescan
```

# Blacklists

The plugin supports configuring a list of channels to be blacklisted. This
inhibits either all message handling or specific message handling.

- `phabricator.blacklist` disables all message handling
- `phabricator.url_blacklist` disables handling of URLs (keywords remain
  active - e.g. `https://phabricator.kde.org/D123` is not handled but
  `D123` outside a URL still is)

# Testing

API interaction is recorded through VCR and stripped off API tokens. To actually
set a token for recording make sure to define PHABRICATOR_API_TOKEN in the env.
