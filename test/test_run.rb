# frozen_string_literal: true

# SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
# SPDX-FileCopyrightText: 2016-2019 Harald Sitter <sitter@kde.org>

# This is a fancy wrapper around test_helper to prevent the collector from
# loading the helper twice as it would occur if we ran the helper directly.
require_relative 'test_helper'

Test::Unit::AutoRunner.run(true, File.absolute_path(__dir__))
