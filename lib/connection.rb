# frozen_string_literal: true

# SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
# SPDX-FileCopyrightText: 2016 Harald Sitter <sitter@kde.org>

require 'faraday'
require 'json'
require 'uri'
require 'logger'

require_relative 'representation'

module Conduit
  # Connection logic wrapper.
  class Connection
    # FinerStruct wrapper around returned hash.
    class ConduitRepsonse < FinerStruct::Immutable
      def initialize(response)
        super(JSON.parse(response.body, symbolize_names: true))
      end

      def to_error
        RuntimeError.new("ConduitError #{error_code}: #{error_info}")
      end

      def error?
        !error_code.nil?
      end
    end

    class << self
      # Somewhat hackish since we have no global config.
      attr_accessor :api_token
    end

    attr_accessor :uri

    def initialize
      @uri = URI.parse('https://phabricator.kde.org/api')
    end

    def call(meth, **kwords)
      response = get(meth, kwords)
      raise "HTTPError #{response.status}" if response.status != 200
      response = ConduitRepsonse.new(response)
      raise response.to_error if response.error?
      response.result
    end

    private

    def get(meth, **kwords)
      client.get do |req|
        req.url meth
        req.headers['Content-Type'] = 'application/json'
        req.params = kwords.merge('api.token' => api_token)
      end
    end

    def api_token
      @api_token ||= ENV.fetch('PHABRICATOR_API_TOKEN', self.class.api_token)
    end

    def client
      @client ||= Faraday.new(@uri.to_s) do |faraday|
        faraday.request :url_encoded
        # faraday.response :logger, ::Logger.new(STDOUT), bodies: true
        faraday.adapter Faraday.default_adapter
      end
    end
  end
end
