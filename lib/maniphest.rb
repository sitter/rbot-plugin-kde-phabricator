# frozen_string_literal: true

# SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
# SPDX-FileCopyrightText: 2016 Harald Sitter <sitter@kde.org>

require_relative 'connection'
require_relative 'representation'

module Conduit
  # A task
  class Maniphest < Representation
    class << self
      def get(task_id, connection = Connection.new)
        new(connection, connection.call('maniphest.info', task_id: task_id))
      end
    end
  end
end
