# frozen_string_literal: true

# SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
# SPDX-FileCopyrightText: 2016 Harald Sitter <sitter@kde.org>

require 'finer_struct'

module Conduit
  # Base representation class to coerce transactional types into useful
  # objects.
  class Representation < FinerStruct::Immutable
    # @!attribute connection
    #   @return [Connection] the connection used for instance operations
    attr_accessor :connection

    # Initialize a new representation
    # @param connection [Connection] connection to use for instance operations
    # @param hash [Hash] native hash to represent
    def initialize(connection, hash = {})
      @connection = connection
      super(hash)
    end
  end
end
