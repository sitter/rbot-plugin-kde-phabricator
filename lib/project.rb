# frozen_string_literal: true

# SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
# SPDX-FileCopyrightText: 2016 Harald Sitter <sitter@kde.org>

require_relative 'connection'
require_relative 'representation'

module Conduit
  # A project
  class Project < Representation
    class << self
      def find_by_phids(phids, connection = Connection.new)
        return [] if !phids || phids.empty?
        page = connection.call('project.query', phids: phids)
        # In the data: keys are the phids, values are the actual objects
        page[:data].values.collect do |hash|
          new(connection, hash)
        end
      end
    end
  end
end
