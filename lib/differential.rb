# frozen_string_literal: true

# SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
# SPDX-FileCopyrightText: 2016 Harald Sitter <sitter@kde.org>

require_relative 'connection'
require_relative 'representation'

module Conduit
  # A code review
  class Differential < Representation
    class << self
      def get(id, connection = Connection.new)
        data = connection.call('differential.query', ids: [id])
        raise 'Empty response from Phabricator' if !data || data.empty?
        new(connection, data[0])
      end
    end
  end
end
